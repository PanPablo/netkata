$( document ).ready(function() {


    //Mobile Menu
    $(".hamburger").click(function() {
        $(".header__menu").toggle();
        $(".hamburger").toggleClass("open");
    });

    //Slider
    $(document).ready(function(){
        $('.slider').slick({
            slidesToShow: 1,
            autoplay: true,
            arrows: false,
            dots: true,
            pauseOnHover: false,
        });
    });

});
